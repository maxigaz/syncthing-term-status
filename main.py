import requests
import json
import sys

from textual.app import App, ComposeResult
from textual import events
from textual.binding import Binding
from textual.containers import Container, Horizontal, ScrollableContainer, Vertical
from textual.screen import Screen
from textual.widgets import Button, Header, Input, Footer, Label, Markdown, Static

# Global variables for storing Syncthing URL and API key
st_url = ""
api_key = ""

# Initial test response of REST request
test_response = ""

class StatusDisplay(ScrollableContainer):
    """A scrollable container that holds other widgets, used for displaying Syncthing status."""

    def compose(self) -> ComposeResult:
        # Make sure the current device's widget is displayed first
        yield Markdown("", id="thisdevice")

class Vertical(Vertical):
    """One widget for each device."""

    # Initial state
    hidden = True

    def on_button_pressed(self, event: Button.Pressed) -> None:
        """Actions triggered by pressing the UI buttons."""
        # hidden = self.hidden
        md = self.query_one("Markdown")
        if not self.hidden:
            md.add_class("hidden")
            self.hidden = True
        else:
            md.remove_class("hidden")
            md.scroll_visible()
            self.hidden = False

class URLInput(Input):
    """A text input field for entering the URL of a Syncthing instance."""

    def on_key(self, event: events.Key) -> None:
        if event.key == "enter":
            # Stop the key press event from propagating onto the other screen
            event.stop()
            # If there's something wrong with the inputs, don't switch screens, just show a message
            if app.response_test() == "OK":
                app.screenswitch()

class ApiKeyInput(Input):
    """A text input field for entering the Syncthing API key."""

    def on_key(self, event: events.Key) -> None:
        if event.key == "enter":
            event.stop()
            if app.response_test() == "OK":
                app.screenswitch()

class URLInputLabel(Label):
    """A label that appears above URLInput"""

class ApiKeyInputLabel(Label):
    """A label that appears above ApiKeyInput"""

class URLInputError(Label):
    """A label that appears above URLInput"""

class ApiKeyInputError(Label):
    """A label that appears above ApiKeyInput"""

class InputScreen(Screen):
    """A widget that groups together all the inputs and their labels."""

    def compose(self) -> ComposeResult:
        """Create child widgets for InputGroup."""
        yield Header()
        yield Container(
            Horizontal(
                URLInputLabel("Syncthing instance URL:"),
                URLInputError("", classes="errorlabel"),
                classes="inputlabelgroup"
            ),
            URLInput("http://127.0.0.1:8384", placeholder="Enter URL here", classes="inputfield"),
            Horizontal(
                ApiKeyInputLabel("API key:"),
                ApiKeyInputError("", classes="errorlabel"),
                classes="inputlabelgroup"
            ),
            ApiKeyInput("", placeholder="Enter API key here", classes="inputfield"),
            id="inputcont"
        )
        yield Footer()

    def record_inputs(self) -> None:
        """Record the content of the two input fields for the other screen."""
        global st_url
        global api_key
        st_url = self.query_one(URLInput).value
        api_key = self.query_one(ApiKeyInput).value

    def on_mount(self) -> None:
        self.set_key_from_args()

    def set_key_from_args(self) -> None:
        # If there's a command-line argument passed on to the script, fill ApiKeyInput with it
        if len(sys.argv) > 1:
            self.query_one("ApiKeyInput").value = sys.argv[1]

class StatusScreen(Screen):
    """A widget that groups together all the status info."""

    word = ""

    def compose(self) -> ComposeResult:
        """Create child widgets for the StatusScreen."""
        yield Header()
        yield StatusDisplay()
        yield Footer()

    def on_mount(self) -> None:
        """Run this when the screen appears."""
        # Prepare info to be displayed
        self.displayinfo()
        # Update the text content of the widget
        # self.query_one("StatusDisplay").update(self.word)

    def displayinfo(self) -> None:
        """Display status information about a Syncthing instance"""

        # Use Syncthing URL and API Key saved globally
        global st_url
        global api_key

        sd = self.query_one("StatusDisplay")

        # Send GET requests and store their responses
        # response = self.restget(st_url + "/rest/config", api_key)

        config_data = app.restget(st_url + "/rest/config", api_key)
        connnections_data = app.restget(st_url + "/rest/system/connections", api_key)

        status_data = app.restget(st_url + "/rest/system/status", api_key)
        current_device_id = status_data['myID']

        # Check how many known devices we have in total
        totaldevs = len(config_data['devices'])

        # Variable for storing individual info about all known devices
        # Initially have it empty so that data can be appended to it
        # markdown_device_entries = ""

        # Dictionary that stores info about every remote device
        rdevdict = {}

        # Iterate through config_data
        for i in range(0, totaldevs):
            devname = config_data['devices'][i]['name']
            devID = config_data['devices'][i]['deviceID']
            pause_status = config_data['devices'][i]['paused']

            # Since we have devID ready, look it up in the dict with the current connections
            # and show the connection status of the device
            conn_status = connnections_data['connections'][devID]['connected']

            # Also check (device-specific) completion data
            completion_data = app.restget(st_url + "/rest/db/completion", api_key, devID)
            comp_status = f"{completion_data['completion']:.0f}"
            if comp_status == '100':
                comp_status = 'Up to Date'
            else:
                comp_status = 'Out of Sync (' + comp_status + '%)'

            # If this is the current device, save its details separately and move on.
            if devID == current_device_id:
                current_device_name = devname
                current_pause_status = pause_status
                current_comp_status = comp_status

                # Display in a nicely formatted Markdown widget
                markdown_base = f"""
## This Device

### {current_device_name}

**Device ID:** {current_device_id}

**Paused:** {current_pause_status}

**Sync Status:** {current_comp_status}

## Remote Devices ({totaldevs - 1})

"""
                sd.query_one("#thisdevice").update(markdown_base)
                continue

            # Append info per device to table
            rdevdict.update({devname: {
                "ID": devID,
                "connected": conn_status,
                "paused": pause_status,
                "sync": comp_status,
            }})

        # Add Markdown markup for each device
        for k in sorted(rdevdict):
            # Store pure strings in variables for easier access
            str_devname = [k][0]
            str_id = rdevdict[k]['ID']
            str_conn = rdevdict[k]['connected']
            str_paused = rdevdict[k]['paused']
            str_sync = rdevdict[k]['sync']

#             markdown_device_entry_header = f"""
# ### {str_devname}

# """
            # Device details as content for the Markdown widget
            markdown_device_entry_details = f"""
**Device ID:** {str_id}

**Connected:** {str_conn}

**Paused:** {str_paused}

**Sync Status:** {str_sync}

"""

            sd.mount(Vertical(
                Horizontal(
                    Button(str_devname),
                    Static(self.shortdesc(str_paused, str_conn, str_sync), classes="shortdesc")
                ),
                Markdown(markdown_device_entry_details, classes="deviceentry hidden")
            ))

        # self.word = markdown_base + markdown_device_entries

    def shortdesc(self, paused: bool, connected: bool, insync: str) -> None:
        """Shows a short description for the current device."""
        # Check three status parameters, and show one of them as a short description
        if paused:
            return "Paused"
        elif not connected:
            return "Disconnected"
        else:
            return insync

class IncorrectApiKeyException(Exception):
    """Custom exception class for handling incorrect API keys."""
    def __init__(self):
        Exception.__init__(self)

class SyncthingTermStatus(App):
    """An application that shows the current Syncthing status."""

    CSS_PATH = "styles.css"
    TITLE = "Syncthing Term Status"
    BINDINGS = [
        ("c", "screenreset", "Change instance"),
        ("d", "toggle_dark", "Toggle dark mode"),
        Binding("q,ctrl+c", "app.quit", "Quit", show="true"),
    ]
    # SCREENS = {"inputscreen": InputScreen(), "statusscreen": StatusScreen()}

    def on_mount(self) -> None:
        self.install_screen(InputScreen(), name="inputscreen")
        self.push_screen("inputscreen")
        self.query()

    def restget(self, api_url: str, api_key: str, device_id: str = "") -> None:
        """Sends a GET request and returns the response as a Python object."""

        # If device_id (an optional argument) has been specified, add it to the URL;
        # this is required by some request types (e.g. /rest/db/completion)
        if device_id == "":
            response = requests.get(api_url, headers={"X-API-key": api_key})
        else:
            response = requests.get(api_url + "?device=" + device_id, headers={"X-API-key": api_key})

        if response.status_code == 403:
            return "<403>"

        # Get JSON data from the response, validate it, and then turn it into a Python object
        t = json.dumps(response.json())
        return json.loads(t)

    def screenswitch(self) -> None:
        """Function that switches to the second screen, shared by the two text input fields."""
        self.query_one("InputScreen").record_inputs()
        self.install_screen(StatusScreen(), name="statusscreen")
        self.switch_screen("statusscreen")

    def action_screenreset(self) -> None:
        """Action method that resets screens, i.e. switches back to the first screen."""
        self.switch_screen("inputscreen")
        self.uninstall_screen("statusscreen")

    def response_test(self) -> None:
        """Make a test request using the REST API and if there's an error, inform the user about it."""

        url_error_msg = self.query_one("URLInputError")
        key_error_msg = self.query_one("ApiKeyInputError")

        # Initialise labels holding error messages
        url_error_msg.update("")
        key_error_msg.update("")

        # Check if one of the input fields is empty
        if self.query_one(URLInput).value == '':
            url_error_msg.update("Empty input field!")
            return "EmptyURL"
        if self.query_one(ApiKeyInput).value == '':
            key_error_msg.update("Empty input field!")
            return "EmptyAPIKey"

        try:
            self.query_one("InputScreen").record_inputs()
            response_code = self.restget(st_url + "/rest/config", api_key)
            # Raising test
            if response_code == '<403>':
                raise IncorrectApiKeyException
        except requests.exceptions.ConnectionError:
            url_error_msg.update("Unable to connect!")
            return "ConnectionError"
        except requests.exceptions.InvalidURL:
            url_error_msg.update("Invalid URL!")
            return "InvalidURL"
        except requests.exceptions.MissingSchema:
            url_error_msg.update("Missing URL scheme! Perhaps http and https is missing from the beginning?")
            return "MissingSchema"
        # Custom exception
        except IncorrectApiKeyException:
            key_error_msg.update("Incorrect API key!")
            return "IncorrectApiKey"
        else:
            return "OK"

app = SyncthingTermStatus()
statusscreen = StatusScreen()
app.run()
