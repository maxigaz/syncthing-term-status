# CHANGELOG

## 0.3.0 (released on 2023-07-21)

- Show messages about various errors above one of the input fields instead of crashing (e.g. empty input field or invalid URL)
