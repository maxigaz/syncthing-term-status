# Syncthing Term Status

A simple TUI application that shows the current Syncthing status, including the list of known devices and their connection, paused, and sync status.

## Current Project Status

The software is currently considered alpha: it has not been thoroughly tested and significant parts of it may be rewritten at any time. For now, please, only use it for testing purposes!

## How to Install

Download the project files with the latest changes with `git`:

```sh
git clone https://codeberg.org/maxigaz/syncthing-term-status.git
cd syncthing-term-status
```

Create a virtual Python environment and activate it:

```sh
python -m venv .venv
source .venv/bin/activate
```

Install the application's dependencies:

```sh
pip install -r requirements.txt
```

Run with Python:

```sh
python main.py
```

Or run with Textual in development mode:

```sh
textual run --dev main.py
```

## Credits

- [Textual](https://github.com/Textualize/textual)

## Licence

GPLv3
